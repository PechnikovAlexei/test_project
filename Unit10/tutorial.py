import sqlalchemy as sa
import datetime

engine = "sqlite"
#URL = "{engine}:///{user}@{pass}/host/{database}"
URL = "sqlite:///database.sqlite"

engine = sa.create_engine(URL) # Создание подключения
metadata = sa.MetaData() # Создние области данных

products = sa.Table(
    "products",
    metadata,
    sa.Column("id", sa.Integer, primary_key=True),
    sa.Column("name", sa.String(256), nullable=False, unique=True),
    sa.Column("price", sa.Float, nullable=True, unique=False, default=lambda: 0.0),
    sa.Column("created_at", sa.DateTime, default=datetime.datetime.now, nullable=False),
)

statistics = sa.Table(
    "statistics",
    metadata,
    sa.Column("id", sa.Integer, primary_key=True),
    sa.Column("product_id", sa.Integer, sa.ForeignKey("products.id")),
    sa.Column("created_at", sa.DateTime, default=datetime.datetime.now, nullable=False),
)

metadata.create_all(engine) # отправка данных для создания объекта

connection = engine.connect()
#request = "INSERT INTO products ('name', 'price') VALUES ('Водка', 59.90);"
#request = "SELECT * FROM products"
#response = connection.execute(request)
#print(response.fetchall())
#connection.execute(request)
#request = products.insert()
#print(request)
# INSERT INTO products (id, name, price, created_at) VALUES (:id, :name, :price, :created_at)

request = statistics.insert()
connection.execute(request, product_id=1, created_at=datetime.datetime.now())

request = statistics.update().where(statistics.c.id == 1)
connection.execute(request, product_id=0)
