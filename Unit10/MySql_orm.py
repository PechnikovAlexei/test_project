import sqlalchemy as sa
import datetime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
import pymysql

Base = declarative_base() # Аналог Метадаты

class Common: # Класс общих элементов 
    id = sa.Column(sa.Integer, primary_key=True)
    created_at = sa.Column(sa.DateTime, nullable=False, default=datetime.datetime.now)

class Product(Common, Base):
    __tablename__ = "products"
    name = sa.Column(sa.String(64), unique=True, nullable=False)
    price = sa.Column(sa.Float, default=0)

class Statistics(Common, Base):
    __tablename__ = "statistics"
    product_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Product.__tablename__}.id"), nullable=False) # ссылается на класс
    product = relationship(Product, uselist=True) # Берет все строки связанные через ForeignKey из Product
    count = sa.Column(sa.Integer, default=0, nullable=False)
    date = sa.Column(sa.Date, nullable=False, default=datetime.date.today)

URL = "mysql:///admin:admin@localhost/database?encoding=utf8"

engine = sa.create_engine(URL)
Session = sessionmaker() #
Session.configure(bind=engine) #
session = Session() # Настройка подключения


Base.metadata.create_all(engine)
# product = Product(name="Vodka 'Everyday' 0.3", price=59.9)
product1 = Product(name="Vodka3 'Everyday' 0.3", price=59.9) # Создание объекта
# session.add(product)  add_all можно передать кортеж
product2 = Product(name="Bread3 Borodinsky", price=48.9)
session.add_all((product1, product2))
session.commit()

session.delete(product1)
session.delete(product2)
session.commit()

result = session.query(Product).filter(Product.price == 59.9).all() # SELECT по условию
#session.delete_all(result) Удаление выбранного списка
for product in result:
    print(product.id, product.name, product.price)

result = session.query(Product).filter(Product.price == 48.9).first()
result.price = 49.9
session.commit()

session.close()