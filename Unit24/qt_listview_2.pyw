import sys


from PyQt5.QtWidgets import QApplication, QListView, QPushButton, QMainWindow, QLineEdit
from PyQt5.QtGui import QStandardItem, QStandardItemModel


def add_item():
    text = lineedit.text().strip()
    if text:
        model.appendRow(QStandardItem(text))
    lineedit.setText("")

app = QApplication(sys.argv)
main = QMainWindow()
main.setFixedWidth(600)
main.setFixedHeight(600)

listview = QListView(main)
listview.resize(580, 380)
listview.move(10, 10)
model = QStandardItemModel()
listview.setModel(model)
listview.setStyleSheet("font-size: 32px;")

lineedit = QLineEdit(main)
lineedit.resize(450, 50)
lineedit.move(10, 400)
lineedit.setStyleSheet("font-size: 32px;")

button = QPushButton(main)
button.resize(120, 50)
button.move(470, 400)
button.setText("ADD")
button.clicked.connect(add_item)
button.setStyleSheet("font-size: 32px;")


main.show()
sys.exit(app.exec_())