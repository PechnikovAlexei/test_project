import sys

from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import QApplication, QMainWindow

from listview import Ui_MainWindow

def add_item():
    text = ui.lineEdit.text().strip()
    if text:
        ui.listView.model().appendRow(QStandardItem(text))
    ui.lineEdit.setText("")

app = QApplication(sys.argv)
ui = Ui_MainWindow()
main = QMainWindow()
ui.setupUi(main)
ui.pushButton.clicked.connect(add_item)
ui.listView.setModel(QStandardItemModel())
main.show()

sys.exit(app.exec_())