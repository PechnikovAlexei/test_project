# qt.io doc.qt.io
import sys


from PyQt5.QtWidgets import QApplication, QLabel, QMainWindow
from PyQt5.QtCore import QRect, Qt
from PyQt5.QtGui import QFont, QColor

app = QApplication(sys.argv)

main = QMainWindow()
main.setGeometry(QRect(100, 100, 600, 600))
main.resize(450, 450)

font = QFont()
font.setFamily("verdana")
font.setPixelSize(25)

label = QLabel("Hello, world! " * 3, main)
label.move(100, 100)
label.setFont(font)
label.setStyleSheet("color: #fa5a5") # вместо  #fa5a5 rgb(255, 0, 0)
label.adjustSize() # изменяет размер под длину текста
label.setText("Привет, мир! " * 2)

main.show()

exit(app.exec())