import sys

from PyQt5 import uic
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import QApplication, QMainWindow


def add_item():
    text = main.lineEdit.text().strip()
    if text:
        main.listView.model().appendRow(QStandardItem(text))
    main.lineEdit.setText("")

app = QApplication(sys.argv)
main = uic.loadUi("untitled.ui")

main.pushButton.clicked.connect(add_item)
main.listView.setModel(QStandardItemModel())
main.show()

sys.exit(app.exec_())