from tkinter import Label, Entry, Button, Radiobutton, DISABLED
from time import sleep

def click():
    label.config(text=entry.get())


label = Label(font="Arial 20")
entry = Entry(font="Arial 20")
button = Button(text="Button", font="Arial 20", command=click, state=DISABLED)
radio1 = Radiobutton(text="RadioButton", value=0)
radio2 = Radiobutton(text="RadioButton", value=1)
radio1.pack()
radio2.pack()
label.pack()
entry.pack()
button.pack()
label.mainloop()