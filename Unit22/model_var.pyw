from tkinter import StringVar, Toplevel, Entry, Label, Button, mainloop, Tk


def show_dialog():
    dialog = Toplevel(root)
    entry = Entry(dialog, textvariable=string_var, font="Arial 20")
    entry.pack()
    

def render():
    label.config(text=string_var.get())


root = Tk()
string_var = StringVar()
button1 = Button(root, text="Click Me", font="Arial 40", command=show_dialog)
button2 = Button(root, text="Render!", font="Arial 40", command=render)
label = Label(root, text="Empty", font="Arial 20")
label.pack()
button1.pack()
button2.pack()
mainloop()
