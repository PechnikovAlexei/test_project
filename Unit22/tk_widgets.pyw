from tkinter import Label, Entry, Button


def click():
    global font_size
    font_size += 4
    label.config(font = f"Arial {font_size}")
    button.config(font = f"Arial {font_size}")



font_size = 12
label = Label(text="Label", font=f"Arial {font_size}")
button = Button(text="Button", font=f"Arial {font_size}", command=click)
label.pack()
button.pack()
label.mainloop()