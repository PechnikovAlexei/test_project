from tkinter import Button, Tk, mainloop, messagebox, Toplevel


def show_dialog():
    top = Toplevel(root)
    top.transient(root)
    top.grab_set()
    top.focus_set()
    # messagebox.askokcancel("Title", "Body") # showerrore, showwarning, showinfo, askyesno(возвращает True или False)
    # messagebox.askyesnocancel("Title", "Body")
    top.wait_window() 


root = Tk()
button = Button(text="Click me, please", command=show_dialog)
button.pack()
mainloop()