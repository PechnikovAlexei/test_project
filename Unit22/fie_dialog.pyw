from tkinter import Button, Tk, mainloop, messagebox, Toplevel, filedialog


def show_dialog():
    file_name = filedialog.askopenfilename(
        filetypes=(
            ("TXT files", "*.txt"),
            ("HTML files", "*.htm;*.html"),
            ("All files", "*.*"),
        ),
    )
    print(file_name)


root = Tk()
button = Button(text="Click me, please", command=show_dialog)
button.pack()
mainloop()