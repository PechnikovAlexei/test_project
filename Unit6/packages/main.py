import math

end = "\n" + "-" * 40 + "\n"

print(dir(math), end=end)

print("SQRT:", math.sqrt(25), end=end)

from math import sin, cos

a = 45
print("MUST BE 1:", sin(a) ** 2 + cos(a) ** 2, end=end)


from math import asin as _asin, cos # переименовать

def asin(a):
    return a ** math.e

print("ASIN:", asin(0.25), "_ASIN:", _asin(0.25), end=end)

from math import * # использовать все переменные

print("PI:", pi, end=end)

import calc
print(dir(calc), end=end)

import calc.ops
print("DIV:", calc.ops.div(81, 9), end=end)

from calc import ops
print("DIV:", ops.div(100, 10), end=end)

from calc.ops import div
print("DIV:", div(121, 11), end=end)

import sys
print("PATH:", sys.path, end=end)