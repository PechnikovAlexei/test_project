lst = [1, 2, 3, 4, 5, 6]

it = lst.__iter__() # Запрос метода итераций
print(it.__next__())
print(it.__next__())
print(it.__next__())
print(it.__next__())
print(it.__next__())
print(it.__next__())
print(it.__next__())

it = iter(lst)
print(next(it))
print(next(it))
print(next(it))
print(next(it))
print(next(it))
print(next(it))