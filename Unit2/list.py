# Создание пустого списка
lst1 = []
lst2 = list()

# Создание списка

lst3 = [1, 2.5, True, "string", [None, False]]
lst4 = list("hello")
print(lst4)

print(lst3[1])
lst3[2] = "bool" # Замена элемента списка
print(lst3)
del lst3[4] # Удаляет элемент списка
print(lst3)
print(len(lst3))

print(lst3[-1]) - # Идет выборка с конца

print([1, 2] + [3, 4]) # Соединение списков
print([1, 2] * 3) # Умножение на целые числа

print(lst3.index("bool"))
lst3.append([None, False]) # Добавление в конец весь элемент
print(lst3)
lst3.extend([None, False]) # Добавление в конец по элементу
print(lst3)
print(lst3[4][1])


# Срезы

print(lst3[1:]) # Все эелементы с 1-го
print(lst3[:3]) # Все эелементы до 3-го
print(lst3[5:1:-1]) # С 5-го элемента до 1-го с шагом -1
print(lst3[::-1]) # Вернет список наоборот