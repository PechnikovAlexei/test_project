# Словарь

dct1 = {}
dct2 = dict()

dct3 = {"1": 1, "2": 4, "3": 9}
dct4 = dict([("1", 1), (2, 4), ("3", 9)]) # Передача через dict список, состоящий из кортежей

print(dct3["2"])
print(dct4[2])
dct3["3"] = 10 # Изменяет значение под ключом 3
dct3{"4"} = 16 # Создает новый ключ со значением 16
del dct4[2] # удаление ключа и значение