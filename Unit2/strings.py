# Создание пустых строк
string1 = ''
string2 = ""
string3 = ''''''
string4 = """"""
string5 = str()

# Создание строк
string6 = 'Hello\nWorld!'
string7 = "Hello\nWorld!"
string8 = '''Hello 
World!''' # Переносит строку
string9 = """Hello
World!""" # Переносит строку

print(string9[5])
print(len(string9))
# string9[4] = " " - будет ошибка

print("Hello," + " World!")
print("NO! " * 3)

print(string9.find("l"))  # Выведет первый найденный символ