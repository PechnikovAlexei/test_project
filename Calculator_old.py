print("""Программа Калькулятор
Программа производит операции с двумя числами x и y.
Необходимо ввести два числа и желаемую операцию на выбор:
1. Сложение: +
2. Вычитание: -
3. Умножение: *
4. Деление: /
5. Возведение x в степень y: **
6. Просмотр истории операций: history
7. Выход: exit\n""")

f = open("History.txt", mode="at+")
while True:
    operation = input("Введите операцию")
    if operation not in ("history", "exit"):
        x = int(input("Введите первое число x:"))
        y = int(input("Введите второе число y:"))
        if operation == "+":
            result = x + y
            print("Результат сложения", x, "и", y, ":", result, sep=" ")
            print("Результат сложения", x, "и", y, ":", result, sep=" ", file=f)
            # f.writelines(["Результат сложения ", x, " и ", y, " : ", result])
        elif operation == "-":
            result = x - y
            print("Результат вычитания", x, "и", y, ":", result, sep=" ")
            print("Результат вычитания", x, "и", y, ":", result, sep=" ", file=f)
            # f.writelines(["Результат вычитания ", x, " и ", y, " : ", result])
        elif operation == "*":
            result = x * y
            print("Результат умножения", x, "на", y, ":", result, sep=" ")
            print("Результат умножения", x, "на", y, ":", result, sep=" ", file=f)
            # f.writelines(["Результат умножения ", x, " на ", y, " : ", result])
        elif operation == "/":
            result = x / y
            print("Результат деления", x, "на", y, ":", result, sep=" ")
            print("Результат деления", x, "на", y, ":", result, sep=" ", file=f)
            # f.writelines(["Результат деления ", x, " на ", y, " : ", result])
        elif operation == "**":
            result = x ** y
            print("Результат возведения", x, "в степень", y, ":", result, sep=" ")
            print("Результат возведения", x, "в степень", y, ":", result, sep=" ", file=f)
            # f.writelines(["Результат возведения ", x, " в степень ", y, " : ", result])
    elif operation == "history":
        #f.close()
        #f = open("History.txt", mode="at+")
        f.seek(0)
        print(f.read())
    elif operation == "exit":
        break
    else:
        print("Такой операции нет, выберите из предложенного списка")
f.close()

