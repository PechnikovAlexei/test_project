nk = [int(x) for x in input().split(" ")]
nlst = [int(x) for x in input().split(" ")]
nlst.sort()
klst = [int(x) for x in input().split(" ")]


for x in klst:
    left = 0
    right = len(nlst) - 1
    while (right - left) > 1:
        i = left + (right - left) // 2
        if x < nlst[i]:
            right = i
        else:
            left = i
    a = min([(abs(x - nlst[j]), nlst[j], j) for j in (i - 1, i, i + 1)])
    print(a[1])
