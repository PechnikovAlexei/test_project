

lst = [int(x) for x in input().split(" ")]
graph = []

for x in range(0, lst[0]):
    x = [int(i) for i in input().split(" ")]
    graph.append(x)

def width(start, target): # поиск в ширину
    visited = set()
    queue = [start]

    while queue:
        vertex = queue.pop(0)
        print(vertex)
        visited.add(vertex)

        for v, value in enumerate(graph[vertex]):
            if value == 1:
                if v == target:
                    return True
                elif v not in visited:
                    queue.append(v)
    
    return False

width(lst[1], )
#print(graph)