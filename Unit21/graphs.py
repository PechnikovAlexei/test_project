graph = {
    "A": ("A", "D"),
    "B": ("A",),
    "C": ("B", "D"),
    "D": ("E",),
    "E": (),
    "F": ("G",),
    "G": ("F",),
}


def depth_recursive(node, target, visited=None): # поиск в глубину
    print(node)
    if visited is None:
        visited = set()
    visited.add(node)

    if node == target:
        return True
    
    for vertex in graph[node]:
        if vertex not in visited:
            if depth_recursive(vertex, target, visited):
                return True
    
    return False


def depth(start, target):
    visited = set()
    stack = [start]

    while stack:
        vertex = stack.pop()
        print(vertex)
        visited.add(vertex)

        for v in graph[vertex]:
            if v == target:
                return True
            elif v not in visited:
                stack.append(v)
            
    return False

print(depth("A", "E"))
print(depth("C", "G"))
