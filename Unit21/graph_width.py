graph_python = {
    "A": ("A", "D"),
    "B": ("A",),
    "C": ("B", "D"),
    "D": ("E",),
    "E": (),
    "F": ("G",),
    "G": ("F",),
}

graph = (
    (1, 0, 0, 1, 0, 0, 0),
    (1, 0, 0, 0, 0, 0, 0),
    (0, 1, 0, 1, 0, 0, 0),
    (0, 0, 0, 0, 1, 0, 0),
    (0, 0, 0, 0, 0, 0, 0),
    (0, 0, 0, 0, 0, 0, 1),
    (0, 0, 0, 0, 0, 1, 0),
)

def width_python(start, target): # поиск в ширину
    visited = set()
    queue = [start]

    while queue:
        vertex = queue.pop(0)
        print(vertex)
        visited.add(vertex)

        for v in graph_python[vertex]:
            if v == target:
                return True
            elif v not in visited:
                queue.append(v)
    
    return False


def width(start, target): # поиск в ширину
    visited = set()
    queue = [start]

    while queue:
        vertex = queue.pop(0)
        print(vertex)
        visited.add(vertex)

        for v, value in enumerate(graph[vertex]):
            if value == 1:
                if v == target:
                    return True
                elif v not in visited:
                    queue.append(v)
    
    return False


print(width(0, 4))
print(width(2, 5))
