class Animal:
    def __init__(self, name):
        self.name = name
    
    def sound(self):
        print("Any sound")

    def eat(self, weight):
        print(f"{self.name} eat {weight} kg of food")

class Cow(Animal):
    def sound(self):
        print(f"Mooo! - say {self.name}")

murka = Cow("Murka")
murka.sound()
