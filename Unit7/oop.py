class Animal: # создание класса
    pass

a1 = Animal()
a2 = Animal()
print(dir(a1))
print("-" * 40)
print(a1.__class__, Animal, a1.__class__ is Animal)
print("-" * 40)
print(isinstance(a1, Animal)) # Проверка по дереву классов