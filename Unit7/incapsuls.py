class Animal: # Внутри класса можно писать любой код
    weight = 200
    color = "red"

    print("Hello")

a1 = Animal()
a2 = Animal()
print(dir(a1))
print("-" * 40)
print("Animal:", Animal.color, Animal.weight)
print("A1:", a1.color, a1.weight)
print("A2:", a2.color, a2.weight)
print("-" * 40)

a1.color = "green"
a2.weight = 400
print("A1:", a1.color, a1.weight)
print("A2:", a2.color, a2.weight)
print("-" * 40)

a1.size = 600 # Добавление аттрибутов, не сост. в классе
a2.size = 1200
print("A1:", a1.color, a1.weight, a1.size)
print("A2:", a2.color, a2.weight, a2.size)
print("-" * 40)

del a1.size # Удаление аттрибута
print(dir(a1))
print("-" * 40)

Animal.weight = 50 # Переназначили аттрибуты класса
Animal.color = "blue"
print("A1:", a1.color, a1.weight)
print("A2:", a2.color, a2.weight, a2.size)
print("-" * 40)
