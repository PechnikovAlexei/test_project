class Animal:
    weight = 200
    color = "red"

    def __init__(self, name): # Конструктор - описагие создания уникального аттрибута объекта
        self.name = name
    def print_weight(self, word): # Определение метода
        print(f"Weight: {self.name}: {self.weight}")
    def __del__(self): # деструктор - описание логики удаления аттрибута
        print(f"Deleted {self.name}")

a1, a2 = Animal("Murka"), Animal("Bor'ka") # присваивание уникального аттрибута
a1.weight = 300
a1.print_weight("opa")
a2.print_weight("opa")

Animal.print_weight(a1, "opa")