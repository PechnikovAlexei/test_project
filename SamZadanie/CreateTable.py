class GenField:
    def __init__(self, name, ftype, min_length, max_length):
        self.name = name
        self.ftype = ftype
        self.min_length = min_length
        self.max_length = max_length

    def __str__(self):
        return 'Field name: %s (%s)' % (self.name, self.ftype)


class GenTable:
    def __init__(self, name, count_record, *fields):
        self.name = name
        self.count = count_record
        self.fields = fields[:]

    def __str__(self):
        return 'Generate for table %s count %d' % (self.name, self.count)

    def get_fields(self):
        for field in self.fields:
            print(field.name)


field1 = GenField('id', 'int', 1, 1000)
field2 = GenField('name', 'str', 10, 20)
field3 = GenField('is_delete', 'int', 0, 1)
field4 = GenField('id_parent', 'int', 0, 1000)

print(field1)

field_list = []
field_list.append(field1)
field_list.append(field2)
field_list.append(field3)
field_list.append(field4)

table = GenTable('city', 100, *field_list)
print(table)
table.get_fields()