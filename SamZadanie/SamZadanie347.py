N = int(input())
a = list()

for i in range(0, N):
    a.append(int(input()))

print("YES" if 0 in a else "NO")
