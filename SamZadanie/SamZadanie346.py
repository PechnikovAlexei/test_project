N = int(input())
a = list()
zero, pos, neg = 0, 0, 0

for i in range(0, N):
    a.append(int(input()))

for i in a:
    if i == 0:
        zero += 1
    elif i < 0:
        neg += 1
    else:
        pos += 1

print(zero, pos, neg, sep=" ")
