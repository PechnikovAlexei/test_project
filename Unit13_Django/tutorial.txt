1. virtualenv --python=python --prompt="(twitter) " venv
2. .\venv\Scripts\activate.bat
3. django-admin startproject twitter . # запуск проекта с названием twitter в текущем каталоге (.)

4. (twitter) C:\Intel\Profiles\Project\twitter>python manage.py runserver
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).

You have 17 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.
Run 'python manage.py migrate' to apply them.
November 01, 2019 - 19:22:00
Django version 2.2.6, using settings 'twitter.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CTRL-BREAK.

5. запущен проект по адресу http://localhost:8000/
6. python manage.py runserver 80
7. в settings переменная templates, где указывается каталог с шаблонами
8. {% extends "base.html" %} - наследование шаблона