import requests

city_name = input("Введите название города: ")
# country_code = input("Введите код страны, напрмер GB: ")

# response = requests.request(
#     "GET",
#     f"http://api.openweathermap.org/data/2.5/weather?q={city_name},{country_code}",
#     params={"appid": "5d1c5370369029f2d3d9274729db73b2",}
# )

response = requests.request(
    "GET",
    "http://api.openweathermap.org/data/2.5/weather?",
    params={"appid": "5d1c5370369029f2d3d9274729db73b2", "units": "metric", "q": city_name}
)


# print(response.json())

data = response.json()

#weather = (response.json().get("main").get("temp"))
print("Текущая температура:", data['main']['temp'], "градусов Цельсия")

#windspeed = (response.json().get("wind").get("speed"))
print("Скорость ветра:", data['wind']['speed'], "м/с")

winddeg = data['wind']['deg']

if winddeg == 0:
    print("Направление ветра: Северное")
elif winddeg in range(1, 90):
    print("Направление ветра: Северо-Восточное")
elif winddeg == 90:
    print("Направление ветра: Восточное")
elif winddeg in range(91, 180):
    print("Направление ветра: Юго-Восточное")
elif winddeg  == 180:
    print("Направление ветра: Южное")
elif winddeg in range(181, 270):
    print("Направление ветра: Юго-Западное")
elif winddeg == 270:
    print("Направление ветра: Западное")
else:
    print("Направление ветра: Северо-Западное")
