import requests # библиотека обращения к удаленным ресурсам методом http

response = requests.request(
    "GET", # метод
    "https://informatics.msk.ru", # URL
)
print("STATUS CODE:", response.status.code)
print("HEADERS:", response.headers)
print("BODY:", response.content) # формат bytes
print("BODY:", response.text) # текст
#print("BODY:", response.json()) # текст и json
