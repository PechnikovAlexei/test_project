from django.contrib.auth import login
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, reverse
from django.views import View

class IndexView(View):
	def get(self, request):
		return render(request, "index.html")

class AuthView(View):
	def get(self, request):
		return render(request, "auth.html")

    def post(self, request):
		if request.POST["action"] == "signin":
			return self.signin(request)
		elif request.POST["action"] == "signup":
			return self.signup(request)

    def signin(self, request):
		pass

	def signup(self, request):
		user = User.object.create_user(
			username=request.POST["username"],
			password=request.POST["password"],
		)
		login(request, user)
		return redirect(reverse, "index")
