f = open("wt.txt", mode="wt", encoding="UTF-8")

f.write("Hello, world")
f.write("Привет, мир")
f.close

f = open("wt.txt", mode="wt", encoding="UTF-8")
print("Hello, world!", file=f)
print("Привет, мир!", file=f)
f.close

f = open("wt.txt", mode="wt", encoding="UTF-8")
f.writelines(["Hello, world!\n", "Привет, мир!\n"])
f.close

