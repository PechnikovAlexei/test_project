f = open("./rt.txt", mode="rt", encoding="UTF-8") # Указать путь к файлу
print(f.read())
print("-" * 40)

f.close()
f = open("./rt.txt", mode="rt")
print(f.read(8))
print(f.read(6))
print("-" * 40)
f.close()

f = open("./rt.txt", mode="rt")
print(f.readline())
print(f.readline())
print("-" * 40)
f.close()

f = open("./rt.txt", mode="rt")
print(f.readline(8))
print(f.readline(8))
print(f.readline(8))
print("-" * 40)
f.close()

f = open("./rt.txt", mode="rt")
print(f.readlines())
print("-" * 40)
f.close()

f = open("./rt.txt", mode="rt")
print(f.readlines(16))
print(f.readlines(8))
print("-" * 40)
f.close()

f = open("./rt.txt", mode="rt")
for line in f:
    print(line)
print("-" * 40)
f.close()