f = open("plus.txt", mode="wt+", encoding="UTF-8")

f.write("Hello, world!\n")
print(f.tell()) # Возвращает место курсора
f.seek(0) # Перевод курсора в начало строки
print(f.read())
f.close

f = open("plus.txt", mode="rt+", encoding="UTF-8")

f.write("POP")
print(f.read())
f.write("Привет, мир!\n")
f.close