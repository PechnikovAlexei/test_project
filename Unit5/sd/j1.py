import json

obj = {"string": 1, "true": 2.5, None: [False]}

data = json.dumps(obj)
print(data)

new_obj = json.loads(data)
print(new_obj)