import pickle

obj = {"string": 1, "true": 2.5, "None": [range(20), b"123"]}
print("obj:", obj)

with open("obj.pkl", mode="wb") as f:
    pickle.dump(obj, f)