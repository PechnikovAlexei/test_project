import json

obj = {"string": 1, "true": 2.5, None: [False]}
with open("obj.json", "wt") as f:
    json.dump(obj, f, indent=4)