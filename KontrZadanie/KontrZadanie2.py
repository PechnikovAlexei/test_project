import sqlalchemy as sa
import datetime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = sa.create_engine("sqlite:///database2.sqlite")
Base = declarative_base()


class Common:
    id = sa.Column(sa.Integer, primary_key=True)


class Tours(Common, Base):
    __tablename__ = "tours"
    country = sa.Column(sa.String(64), unique=True, nullable=False)
    price = sa.Column(sa.Float, default=0)
    date_start = sa.Column(sa.Date, nullable=False, default=datetime.date.today)
    date_finish = sa.Column(sa.Date, nullable=False, default=datetime.datetime.today() - datetime.timedelta(days=1))


class Guest(Common, Base):
    __tablename__ = "guests"

    name = sa.Column(sa.String(256), nullable=False)


class Role:
    def __init__(self):
        self.engine = sa.create_engine("sqlite:///database2.sqlite")
        Session = sessionmaker()
        Session.configure(bind=self.engine)
        self.session = Session()

        Base.metadata.create_all(self.engine)

    def get_schedule(self):
        return self.session.query(Guests).all()


class GuestRole(Role):
    def register(self, name):
        guest = Guest(name=name)
        self.session.add(guest)
        self.session.commit()
        self.record = guest

    def cancel(self):
        self.session.delete(self.record)
        self.session.commit()


class OrganizerRole(Role):
    def register_guest(self, name):
        guest = Guest(name=name)
        self.session.add(guest)
        self.session.commit()

    def cancel_guest(self, id):
        guest = self.session.delete(Guest).where(Guest.id == id).first()


Session = sessionmaker()
Session.configure(bind=engine)
session = Session()
Base.metadata.create_all(engine)
tour1 = Tours(country="Egypt", price=60, date_start=datetime.datetime.strptime('20200101', '%Y%m%d').date(),
              date_finish=datetime.datetime.strptime('20200201', '%Y%m%d').date())
tour2 = Tours(country="Russia", price=1,  date_start=datetime.datetime.strptime('20191230', '%Y%m%d').date(),
              date_finish=datetime.datetime.strptime('20200110', '%Y%m%d').date())

session.add_all((tour1, tour2))
session.commit()

session.close()
