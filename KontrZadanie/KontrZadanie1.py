import math

class Figure:
    def __init__(self, name):
        self.name = name
    def ploschad(self, storona1, storona2):
        print(f"Площадь {self.name}", storona1 * storona2)
    def perimetr(self, storona1, storona2):
        print(f"Периметр {self.name}", 2 * (storona1 + storona2))
    def info(self, storona1, storona2):
        print(f"Фигура {self.name} со сторонами {storona1}, {storona2}")

class Rectangle(Figure):
    def __init__(self, name, storona1, storona2):
        super().__init__(name)
        self.storona1 = storona1
        self.storona2 = storona2
    def ploschad(self):
        print(f"Площадь {self.name}", self.storona1 * self.storona2)
    def perimetr(self):
        print(f"Периметр {self.name}", 2 * (self.storona1 + self.storona2))
    def info(self):
        print(f"Фигура {self.name} со сторонами {self.storona1}, {self.storona2}")

class Circle(Figure):
    def __init__(self, name, radius):
        super().__init__(name)
        self.radius = radius
    def ploschad(self):
        print(f"Площадь {self.name}", 3.14 * self.radius ** 2)
    def perimetr(self):
        print(f"Периметр {self.name}", 2 * 3.14 * self.radius)
    def info(self):
        print(f"Фигура {self.name} с радиусом {self.radius}")

class Triangle(Figure):
    def __init__(self, name, storona1, storona2, storona3):
        super().__init__(name)
        self.storona1 = storona1
        self.storona2 = storona2 
        self.storona3 = storona3
    def ploschad(self):
        p = (self.storona1 + self.storona2 + self.storona3) / 2
        result = math.sqrt((p * (p - self.storona1) * (p - self.storona2) * (p - self.storona3)))
        print(f"Площадь {self.name}: {result}")
    def perimetr(self):
        print(f"Периметр {self.name}", self.storona1 + self.storona2 + self.storona3)
    def info(self):
        print(f"Фигура {self.name} со сторонами {self.storona1}, {self.storona2}, {self.storona3}")

figure_list = [["Triangle", 3, 5, 6], ["Circle", 5], ["Rectangle", 3, 5]]

triangle = Triangle(*figure_list[0])
# triangle = Triangle("Triangle", 3, 5, 6)
triangle.ploschad()
triangle.perimetr()
triangle.info()
print("-" * 40)
circle = Circle(*figure_list[1])
# circle = Circle("Circle", 5)
circle.ploschad()
circle.perimetr()
circle.info()
print("-" * 40)
rectangle = Rectangle(*figure_list[2])
# rectangle = Rectangle("Rectangle", 3, 5)
rectangle.ploschad()
rectangle.perimetr()
rectangle.info()
