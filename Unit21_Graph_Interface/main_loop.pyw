from tkinter import mainloop, Label, Tk, Entry, Checkbutton, Button

tk1 = Tk()
tk2 = Tk()
#tk = Tk() # Создание главного окна (необязательно)
#tk.geometry("500x500+0+0") # размер и координаты окна
label1 = Label(tk1, text="Label1", font="Arial 20")
label2 = Label(tk2, text="Label2", font="Arial 20")
label1.pack() # Менеджер компановки
label2.pack()
label1.config(text="EditedLabel1") # Изменение лэйбла
Entry(tk2, width=100).pack() # строка ввода
Checkbutton(tk1, text="Check", font="Arial 20").pack()
Button(tk2, text="Button", height=5, width=10).pack()
tk1.mainloop()