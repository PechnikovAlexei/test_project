import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import requests


class Window1(QWidget):

    def __init__(self):
        super(Window1, self).__init__()
        self.setWindowTitle('Registr')
        self.setFixedWidth(400)
        self.setFixedHeight(200)
        self.setStyleSheet("color: rgb(191, 93, 39)")
        self.setStyleSheet("background-color: rgb(246, 237, 179)")

        self.info = []
        self.button = QPushButton(self)
        self.button.setText('Ok')
        self.button.move(170, 150)
        self.button.clicked.connect(lambda : self.info.append(get_info()))
        self.button.setStyleSheet("background-color: rgb(191, 93, 39)")
        self.button.show()


        ledit1 = QLineEdit(self)
        ledit1.resize(200, 50)
        ledit1.move(100, 30)
        ledit1.setText('Name')
        ledit1.setMaxLength(25)
        ledit1.setStyleSheet("font: Verdana; font-size:50 px")

        ledit2 = QLineEdit(self)
        ledit2.resize(200, 50)
        ledit2.move(100, 90)
        ledit2.setText('Where are you?')
        ledit2.setMaxLength(25)
        ledit2.setStyleSheet("font: Verdana; font-size:50 px")

        def get_info():
            info = [ledit1.text(), ledit2.text()]
            return info


class Window2(QWidget):

    def add_in_list(self, text, view):
        if text:
            item = QListWidgetItem()
            #item.setCheckState(Qt.Checked)
            #item.setIcon(QIcon('SilverFern.png'))
            item.setText(text)
            view.addItem(item)
            self.leditp.setText('')
        else:
            pass

    def up_point(self):
        self.point = self.point + 1
        self.label_np.setText(f"{self.point}")
        self.label_np.adjustSize()
        if self.point > 3 and self.point < 7:
            list2 = ['����� ����� ����� ���� �����������', '����� ���-�� �������',
                            '������ �������������', '������ �������', '������ ������� � ������� �������']
            for i in list2:
                self.list_var1.append(i)
                self.cb.addItem(f"{i}")

            font = QFont()
            font.setFamily("Verdana")
            font.setPixelSize(12)

            self.label_di = QLabel("", self)
            self.label_di.move(410, 270)
            self.label_di.resize(150, 110)
            self.label_di.setFont(font)
            self.label_di.setWordWrap(True)
            self.label_di.setStyleSheet("color: rgb(191, 93, 39)")
            self.label_di.setStyleSheet("background-color: rgb(215, 219, 180)")
            self.label_di.setText('''���� �� ����� ������������ �������, ��� ��� � ������� ���� ������ ���������� �������''')
            self.label_di.show()

        elif self.point > 7 and self.point < 9:
            self.label_di.setText('''� �� ����� ������� ���, � �� ������ ��������� �� ������?''')
        elif self.point > 9 and self.point < 11:
            self.label_di.setText('''�����������, ��� �� ���-�� �������. �������� �� ������)) �� � ���� �� ����� ��������������''')
            pixmap = QPixmap('kot.png')
            self.label_im.setPixmap(pixmap)
        elif self.point > 11:
            self.label_di.setText('''� ����� ���, ��� ���������?''')
            pixmap = QPixmap('lis.jpg')
            self.label_im.setPixmap(pixmap)

    def del_from_list(self):
        a = self.view.takeItem(self.view.currentRow())
        if a:
            self.up_point()

    def __init__(self):
        super(Window2, self).__init__()
        self.setWindowTitle('ToDoList')
        self.setFixedWidth(800)
        self.setFixedHeight(600)
        self.setStyleSheet("color: rgb(191, 93, 39)")
        self.setStyleSheet("background-color: rgb(246, 237, 179)")

        self.label_im = QLabel(self)
        pixmap = QPixmap('lis.jpg')
        self.label_im.setPixmap(pixmap)
        self.label_im.move(570, 270)

        font = QFont()
        font.setFamily("bold italic")
        font.setPixelSize(30)

        font1 = QFont()
        font1.setFamily("bold italic")
        font1.setPixelSize(15)

        self.label_p = QLabel("Point: ", self)
        self.label_p.move(20, 20)
        self.label_p.setFont(font)
        self.label_p.adjustSize()
        self.label_p.setStyleSheet("color: rgb(191, 93, 39)")
        self.label_p.setStyleSheet("background-color: rgb(215, 219, 180)")

        self.point = 0
        self.label_np = QLabel(f"{self.point}", self)
        self.label_np.move(110, 20)
        self.label_np.setFont(font)
        self.label_np.adjustSize()
        self.label_np.setStyleSheet("color: rgb(191, 93, 39)")
        self.label_np.setStyleSheet("background-color: rgb(255, 255, 255)")

        self.label_w = QLabel("Weather : ", self)
        self.label_w.move(20, 70)
        self.label_w.setFont(font)
        self.label_w.adjustSize()
        self.label_w.setStyleSheet("color: rgb(191, 93, 39)")
        self.label_w.setStyleSheet("background-color: rgb(215, 219, 180)")

        self.label_nw = QLabel("Look out the window", self)
        self.label_nw.move(20, 150)
        self.label_nw.setFont(font1)
        self.label_nw.resize(600, 30)
        self.label_nw.setStyleSheet("color: rgb(191, 93, 39)")
        self.label_nw.setStyleSheet("background-color: rgb(255, 255, 255)")

        self.view = QListWidget(self)
        self.view.resize(360, 400)
        self.view.move(20, 200)
        self.view.setWindowTitle('ToDO list:')
        self.view.setFont(font1)

        self.leditp = QLineEdit(self)
        self.leditp.resize(320, 40)
        self.leditp.move(390, 550)
        self.leditp.setText('')
        self.leditp.setMaxLength(70)
        self.leditp.setStyleSheet("font: Verdana; font-size:50 px")
        self.leditp.setStyleSheet("background-color: rgb(255, 255, 255)")

        self.buttonp = QPushButton(self)
        self.buttonp.setText('Add at list')
        self.buttonp.resize(90, 50)
        self.buttonp.move(715, 543)
        self.buttonp.clicked.connect(lambda: self.add_in_list(str(self.leditp.text()), self.view))
        self.buttonp.setStyleSheet("background-color: rgb(215, 219, 180)")
        self.buttonp.show()

        self.buttond = QPushButton(self)
        self.buttond.setText('Finish!')
        self.buttond.resize(120, 60)
        self.buttond.move(410, 410)
        self.buttond.setStyleSheet("background-color: rgb(215, 219, 180)")
        self.buttond.show()

        self.label_ch = QLabel("", self)
        self.label_ch.move(400, 10)
        self.label_ch.resize(250, 50)
        self.label_ch.setFont(font1)
        self.label_ch.setWordWrap(True)
        self.label_ch.setStyleSheet("color: rgb(191, 93, 39)")
        self.label_ch.setStyleSheet("background-color: rgb(215, 219, 180)")
        self.label_ch.setText('''�� ������ ��� ��������? � ���� ��� ���� ���-��� ��� ����:''')


        layout = QHBoxLayout()
        self.cb = QComboBox(self)
        self.cb.resize(250, 40)
        self.cb.move(400, 70)
        self.list_var1 = ["����� ���� ���", "���������� ����� ���� ���� �� ���������",
                        "������ ������ ��������, ������", "������� � �����, �� �������", "���� ���������?",
                        "���� ���� ������ ����� � ����", "����� ���� ���� �� �����������"]
        for i in self.list_var1:
            self.cb.addItem(f"{i}")

        self.buttonc = QPushButton(self)
        self.buttonc.setText('I want it!')
        self.buttonc.resize(100, 40)
        self.buttonc.move(670, 70)
        self.buttonc.clicked.connect(lambda: self.add_in_list(str(self.cb.currentText()), self.view))
        self.buttonc.setStyleSheet("background-color: rgb(215, 219, 180)")
        self.buttonc.show()

class MainWindow(QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle('MainWindow')

    def show_window_1(self):
        self.w1 = Window1()
        self.w1.button.clicked.connect(lambda: self.show_window_2(self.w1.info))
        self.w1.button.clicked.connect(self.w1.close)
        self.w1.show()

    def show_window_2(self, info):
        self.w2 = Window2()
        self.w2.info = info

        font = QFont()
        font.setFamily("Verdana")
        font.setPixelSize(30)

        self.w2.label_n = QLabel(f"Hello, {info[0][0]}", self.w2)
        self.w2.label_n.move(580, 200)
        self.w2.label_n.setFont(font)
        self.w2.label_n.adjustSize()
        self.w2.label_n.setStyleSheet("color: rgb(191, 93, 39)")
        self.w2.label_n.setStyleSheet("background-color: rgb(246, 237, 179)")
        self.w2.label_nw.setText(self.wether())

        self.w2.buttond.clicked.connect(self.w2.del_from_list)

        self.w2.show()

    def wether(self):
        try:
            city = self.w2.info[0][1]
            response = requests.get( f"http://api.openweathermap.org/data/2.5/weather?q={city}&APPID=5d1c5370369029f2d3d9274729db73b2")
            wether_info = response.json()
            temp = f"Temperature: {round(wether_info['main']['temp'] - 273)} Celcius  "
            wind = f"Wind speed: {wether_info['wind']['speed']} m/s  "
            w_deg = wether_info['wind']['deg']
            degree = int((w_deg/22.5) + .5)
            carr = ["N","NNE","NE","ENE","E","ESE", "SE", "SSE","S","SSW","SW","WSW","W","WNW","NW","NNW"]
            we_r = f"Direction of the wind: {carr[(degree % 16)]}  "
            return temp + wind + we_r
        except KeyError:
            return " look out the window, bro"

if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MainWindow()
    #w.show()
    w.show_window_1()
    sys.exit(app.exec_())
