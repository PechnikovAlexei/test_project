import pytest


counter = 0


@pytest.fixture
def fixture1():
    return 42

@pytest.fixture
def fixture2():
    return 41


@pytest.fixture
def fixture3():
    global counter
    counter += 1
    return counter


def test_1(fixture1, fixture2):
    assert fixture1 - fixture2 == 1

@pytest.mark.parametrize(
    "arg, result",
    ((38, 4), (-20, 62), (41, 1))
)

def test_2(fixture1, arg, result):
    assert fixture1 - arg == result

def test_3(fixture3):
    assert fixture3 == 1

def test_4(fixture3):
    assert fixture3 == 1

@pytest.mark.asyncio
async def test_5():
    assert 5 == 5
