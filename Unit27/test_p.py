import pytest


@pytest.mark.parametrize(
    "a,b,c",
    (
        (1, 2, 3),
        (2, 4, 6),
        (-2, 2, 0),
        (0.333333333333, 0.666666666667, 1),
    )
)
def test_sum(a, b, c):
    assert a + b == c

@pytest.mark.parametrize(
    "string",
    (
        "1",
        "278",
        "-905",
        "-304.88888",
        "-76y9",
    ),
)
def test_int(string):
    int(string)
