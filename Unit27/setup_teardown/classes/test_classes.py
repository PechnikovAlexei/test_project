class TestA:
    def setup(self):
        print("Common class setup")
    
    def setup_class(Class):
        print("Class class setup")
    
    def setup_method(self, method):
        print("Class method setup")
    
    def test_1():
        assert 1 == 0, "Description"

    def test_2():
        assert 1 == 1

    def teardown(self):
        print("Common class teardown")
    
    def teardown_class(Class):
        print("Class class teardown")
    
    def teardown_method(self, method):
        print("Class method teardown")