from tkinter import Canvas, Tk, Frame


root = Tk()
frame = Frame(root, width=800, height=800)
canvas = Canvas(frame, width=2000, height=2000)

canvas.place(x=0, y=0)
frame.pack(fill="both", expand=True)

canvas.create_line(10, 10, 190, 150)
canvas.create_line(100, 180, 100, 60, fill="green", width=5, arrow="last", dash=(10, 2), 
activefill="red", arrowshape="10 20 10")
canvas.create_rectangle(10, 210, 190, 260)
canvas.create_rectangle(60, 280, 140, 390, fill="yellow", outline="red", width=3, activedash=(5, 4))
canvas.create_polygon(300, 10, 220, 90, 380, 90)
canvas.create_polygon(240, 310, 360, 310, 390, 380, 210, 380, fill="", outline="red")
canvas.create_oval(450, 10, 550, 110, width=2)
canvas.create_oval(410, 320, 590, 390, fill="grey70", outline="orange")
canvas.create_oval(610, 10, 790, 190, fill="lightgrey", outline="white")
canvas.create_arc(610, 10, 790, 190, start=0, extent=45, fill="red")
canvas.create_arc(610, 10, 790, 190, start=180, extent=25, fill="orange")
canvas.create_arc(610, 10, 790, 190, start=240, extent=100, fill="green", style="chord")
canvas.create_arc(610, 10, 790, 190, start=160, extent=-70, fill="green", style="arc", width=5,
 outline="darkblue")
canvas.create_text(100, 235, text="Hello, Tk!", justify="center", font="Verdana 14")

root.mainloop()
