from tkinter import Label, Tk, mainloop, Frame, N, W, E, S


tk=Tk()
frame = Frame(tk)
LABELS_COLOR = ("yellow", "orange", "#98FB98", "lightblue", "white", "red", "blue", "gray", "green")
LABELS = [
    Label(frame, width=12, height=6, bg=color, text=str(index)) for index, color in enumerate(LABELS_COLOR)
]
LABELS[0].grid(columnspan=2) # sticky - прилипляет к какой-либо стороне света
LABELS[1].grid(row=1, sticky=N+W) # padx, pady
LABELS[2].grid(row=1, column=1)
LABELS[3].grid(row=1, column=2, sticky=N+W+E+S)
LABELS[4].grid(row=2, columnspan=2)
LABELS[5].grid(row=2, column=2)
LABELS[6].grid(row=3)
LABELS[7].grid(row=3, column=2)
LABELS[8].grid(row=4)
frame.pack()
# LABELS[0].grid(column=, row=, columnspan=, rowspan=)
mainloop()