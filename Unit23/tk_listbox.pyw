from tkinter import BROWSE, Button, EXTENDED, Scrollbar, END, Tk, mainloop, Listbox, HORIZONTAL, messagebox, MULTIPLE


def message(listbox):
    items = listbox.curselection()
    if items:
        messagebox.showinfo("Your choice", str(listbox.get(items[0])))
    else:
        messagebox.showwarning("No choice", "No choice")


root = Tk()
root.title("Tk Listbox")

scrollbar_y = Scrollbar(width=20)
scrollbar_x = Scrollbar(width=20, orient=HORIZONTAL) # orient -направление

letters = [chr(code) for code in range(65, 91)]
listbox = Listbox(root, yscrollcommand=scrollbar_y.set, xscrollcommand=scrollbar_x.set, font="Arial 20", selectmode=EXTENDED)
for letter in letters:
    listbox.insert(END, letter)

button = Button(text="choice", width=12, height=4, command=lambda: message(listbox))

scrollbar_y.config(command=listbox.yview)
scrollbar_x.config(command=listbox.xview)

button.pack(side="bottom", pady=5, padx=5)
scrollbar_x.pack(side="bottom", fill="x")
scrollbar_y.pack(side="right", fill="y")
listbox.pack(side="left", fill="both", expand=True)

mainloop()