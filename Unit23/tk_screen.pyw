from tkinter import Canvas, Tk, Frame, Button


def start():
    global start_color
    color = start_color
    for angle in range(0, 360, 30):
        canvas.create_arc(0, 0, 600, 600, start=angle, extent=30, fill=color)
        color = "white" if color=="black" else "black"
    start_color = "white" if start_color=="black" else "black"
    canvas.after(250, start)


root = Tk()
frame = Frame(root, width=600, height=600)
canvas = Canvas(frame, width=600, height=600)
button = Button(text="START", command=start)

start_color = "black"

canvas.place(x=0, y=0)
frame.pack(fill="both", expand=True)
button.pack()
root.mainloop()