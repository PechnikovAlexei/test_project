# coding=UTF-8
# Не понимаю, как избавтиться от if-ов для сокращения времени выполнения...

class Queue:
    def __init__(self):
        self._list = []

    def size(self):
        return print(self._list.__len__())

    def push(self, value):
        self._list.append(value)
        print("ok")

    def pop(self):
        if not self._list:
            print("error")
        else:
            print(self._list[0])
            return self._list.pop(0)

    def front(self):
        print("error" if not self._list else self._list[0])

    def clear(self):
        print("ok")
        return self._list.clear()


queue = Queue()
while True:
    # a, b, c = (c for c in input() if c != ' ')
    # a, b = (input() for _ in range(2))
    lst = list(input().split(" "))
    if lst[0] == 'push':
        queue.push(lst[-1])
    elif lst[0] == "pop":
        queue.pop()
    elif lst[0] == "front":
        queue.front()
    elif lst[0] == "size":
        queue.size()
    elif lst[0] == "clear":
        queue.clear()
    elif lst[0] == "exit":
        print("bye")
        break
