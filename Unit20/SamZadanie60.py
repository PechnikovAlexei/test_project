class Deque:
    def __init__(self):
        self._list = []

    def size(self):
        return print(self._list.__len__())

    def push_front(self, value):
        self._list.insert(0, value)
        print("ok")

    def push_back(self, value):
        self._list.append(value)
        print("ok")

    def pop_front(self):
        print(self._list[0])
        return self._list.pop(0)

    def pop_back(self):
        print(self._list[-1])
        return self._list.pop()

    def front(self):
        print(self._list[0])

    def back(self):
        print(self._list[-1])

    def clear(self):
        self._list = []
        print("ok")


deque = Deque()
while True:
    lst = list(input().split(" "))
    if len(lst) == 2:
        value = int(lst[1])
        # print(lst[0], lst[1])
    if lst[0] == 'push_front':
        deque.push_front(value)
    elif lst[0] == 'push_back':
        deque.push_back(value)
    elif lst[0] == "pop_front":
        deque.pop_front()
    elif lst[0] == "pop_back":
        deque.pop_back()
    elif lst[0] == "back":
        deque.back()
    elif lst[0] == "front":
        deque.front()
    elif lst[0] == "size":
        deque.size()
    elif lst[0] == "clear":
        deque.clear()
    elif lst[0] == "exit":
        print("bye")
        break
