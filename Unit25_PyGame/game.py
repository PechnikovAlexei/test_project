# python -m pygame.examples.aliens

import pygame

pygame.init()
sc = pygame.display.set_mode((600, 400))
font = pygame.font.Font(None, 36)
last_tick = 0

play = True
while play: # все события должны быть в цикле
    ticks = pygame.time.get_ticks() - last_tick
    last_tick = pygame.time.get_ticks()
    fps = str(1000 / ticks) if ticks != 0 else "NaN" # Сам текст
    fps_text = font.render(fps, 0, (125, 255, 0))

    sc.fill((0, 0, 0)) # заполнение экрана черным
    sc.blit(fps_text, (50, 50))
    pygame.display.update()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            play = False
            pygame.quit()
            break
    pygame.time.delay(20) # ограничение fps