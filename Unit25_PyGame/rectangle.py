# python -m pygame.examples.aliens

import pygame
import math

pygame.init()
sc = pygame.display.set_mode((600, 400))
font = pygame.font.Font(None, 36)

last_tick = 0

rectangle = {
    "color": (125, 125, 125),
    "size": {
        "width": 200,
        "height": 100,
    },
    "position": {
        "x": 100,
        "y": 100,
    },
}
obj_rectangle = pygame.Rect((
        rectangle["position"]["x"],
        rectangle["position"]["y"],
        rectangle["size"]["width"],
        rectangle["size"]["height"],
))

line1 = ((40, 100), (140, 300))
lines = ((50, 120), (20, 220), (300, 150))
polygon = ((80, 20), (60, 150), (200, 150), (300, 80))
circle = ((300, 300), 50)
ellipse = (400, 100, 100, 50)
arc = (400, 200, 200, 200)

play = True
while play: # все события должны быть в цикле
    ticks = pygame.time.get_ticks() - last_tick
    last_tick = pygame.time.get_ticks()
    fps = str(1000 / ticks) if ticks != 0 else "NaN" # Сам текст
    fps_text = font.render(fps, 0, (125, 255, 0))

    sc.fill((0, 0, 0)) # заполнение экрана черным
    sc.blit(fps_text, (50, 50))

    pygame.draw.rect(sc, rectangle["color"], obj_rectangle)
    pygame.draw.line(sc, (255, 0, 0), *line1, 10)
    pygame.draw.lines(sc, (255, 255, 255), True, lines, 5)
    pygame.draw.polygon(sc, (0, 0, 255), polygon)
    pygame.draw.circle(sc, (0, 220, 0), *circle, 20)
    pygame.draw.ellipse(sc, (134, 82, 200), ellipse)
    pygame.draw.arc(sc, (0, 128, 128), arc, 0, math.pi / 4, 10)

    pygame.display.update()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            play = False
            pygame.quit()
            break
    pygame.time.delay(20) # ограничение fps