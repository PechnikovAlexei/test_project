def recursion(level=0):
    print("level:", level)
    recursion(level + 1)

def fibonacci(index):
    if index in (0, 1):
        return index
    return fibonacci(index - 2) + fibonacci(index - 1)

print(fibonacci(5))
print(fibonacci(7))
