def func1():
    def func2():
        z = 3
        print("LOCAL:", z)

    z = 2
    func2()
    print("NONLOCAL:", z)

z = 1
func1()
print("GLOBAL:", z)