def manyargs(a1, a2, *args):  # Кортеж, куда передаются все позиционные аргументы
    print(a1, a2)
    print(args)
    print("-" * 10)

manyargs(1, 2)
manyargs(1, 2, 3)
manyargs(1, 2, 3, 4)
manyargs(1, 2, 3, 4, 5)