def manykwargs(arg1, *args, arg3=20, **kwargs):
    print("arg1:", arg1)
    print("args:", args)
    print("arg3:", arg3)
    print("kwargs:", kwargs)
    print("-" * 10)

manykwargs(1)
manykwargs(1, 2)
manykwargs(1, 2, 3)
manykwargs(1, 2, 3, arg4=50)
manykwargs(1, 2, 3, arg3=80, arg4=100)
