def edit(lst):
    lst.append(6)
    return lst

lst = [1, 2, 3, 4, 5]
print("Before:", lst)
new_lst = edit(lst)  # привязывается к одному объекту
# new_lst = edit(lst.copy()) создает отдельный объект
print("after global:", lst)
print("after local:", new_lst)
print("is it same", lst is new_lst)

