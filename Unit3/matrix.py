matrix = [[1, 2, 3, 4, 5],
          [2, 4, 6, 8, 9],
          [3, 6, 9, 12, 15],
          [4, 8, 12, 16, 20],
          [5, 10, 15, 20, 25]]

for line in matrix:
    for element in line:
        print(element, end=", ")
    print()

lst = [5, 8, 1, 3, 4]
for e1 in lst:
    for e2 in lst:
        print(e1, e2)