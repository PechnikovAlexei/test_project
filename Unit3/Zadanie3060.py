N = int(input())

y = 1
while y < N:
    y *= 2
print("YES" if y == N else "NO")