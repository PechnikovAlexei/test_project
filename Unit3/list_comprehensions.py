lst = []
for element in range(0, 11):
    lst.append(element)
print(lst)

lst = [x ** 2 for x in range(0, 11)]
print("LIST:", lst)

st = {x ** 2 for x in range(0, 11)}
print("SET:", st)

tpl = (x ** 2 for x in range(0, 11)) # Функция-генератор
print("TUPLE:", tpl)

for i in tpl:
    print(i)

dct = {x: x ** 2 for x in range(11)}
print("DICT:", dct)