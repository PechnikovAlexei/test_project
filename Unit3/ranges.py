lst = [5, 8, 1, 3, 4]
index = 0
while index < len(lst):
    print(index, "=>", lst[index])
    index += 1
print("-" * 40)

for index in range(len(lst)):
    print(index, "=>", lst[index])
print("-" * 40)

for index, element in enumerate(lst):
    print(index, "=>", element) # enumerate возвращает индекс и значение (только списки)
print("-" * 40)