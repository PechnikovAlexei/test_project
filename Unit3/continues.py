lst = [5, 8, 1, 3, 4]
for element in lst:
    if element in (8, 4):
        continue    # Переводит сразу на следующую итерацию
    print(element)


count = 10
while count > 0:
    print(count)
    if count == 8 or count == 4:
        continue
    count -= 1
