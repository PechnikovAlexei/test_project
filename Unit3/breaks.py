while True:
    answer = input()
    if answer == "exit":
        break
    print(answer)
print("END")


answer = ""
while answer != "exit":
    answer = input()
    if answer == "quit":
        break
    print(answer)
else:
    print("BREAK wasn't called")


lst = [5, 8, 1, 3, 4]
found = 6
for element in lst:
    if element == found:
        print("FOUND")
        break
else:
    print("NOT FOUND")

found = 4
for element in lst:
    if element == found:
        print("FOUND")
        break
else:
    print("NOT FOUND")