#������������ ������� ��������
import sqlalchemy as sa
import datetime
from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()
from sqlalchemy.orm import sessionmaker,relationship
Base = declarative_base()

class Common:
    id = sa.Column(sa.Integer, primary_key=True)

class Warehous(Common, Base):
    __tablename__ = "warehous"
    product_name = sa.Column(sa.String(64), unique=True, nullable=False )
    price = sa.Column(sa.Integer, default=0 )
    min_purchase = sa.Column(sa.Integer, nullable=False, default=1)
    count = sa.Column(sa.Integer, nullable=False, default=1)
class Order(Common,Base):
    __tablename__ = "order"
    product_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Warehous.__tablename__}.id"), nullable=False)
    count = sa.Column(sa.Integer, nullable=False, default=0)
    date_deliv =  sa.Column(sa.Date, nullable=False, default=datetime.date.today)
    date_ord = sa.Column(sa.Date, nullable=False, default=datetime.date.today)
class Delivery(Common,Base):
    __tablename__ = "delivery"
    order_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Order.__tablename__}.id"), nullable=False)
    order_cond = sa.Column(sa.String(200), unique=False, nullable=False )
    adress = sa.Column(sa.String(400), unique=True, nullable=False )
    date_deliv =  sa.Column(sa.Date, nullable=False, default=datetime.date.today)

URL = "sqlite:///bd_opt.sqlite"
engine = sa.create_engine(URL)
Session = sessionmaker()
Session.configure(bind=engine)
session = Session()

#product = Warehous(product_name="bird_food", price=40, min_purchase=7, count=100)
#session.add(product)

#order= Order(product_id=1, count=5,date_deliv=datetime.datetime.strptime('20200106', '%Y%m%d').date() )
#session.add(order)
i = 2
res = session.query(Order).filter(Order.id == i).first()

deliv = Delivery(order_id=i, order_cond="Assemble", adress='Smth streer 9 3/4', date_deliv=res.date_deliv )
session.add(deliv)

session.commit()
Base.metadata.create_all(engine)
session.close()
