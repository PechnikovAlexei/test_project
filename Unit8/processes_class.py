from multiprocessing import Process, Lock
import os
from time import sleep

class MyProcess(Process):
    def __init__(self, begin, end, timeout, daemon=False, lock=None):
        super().__init__(daemon=daemon)
        self.end = end
        self.begin = begin
        self.timeout = timeout
        self.lock = lock

    def run(self):
        while True:
            with self.lock:
                if self.begin >= self.end:
                    break
                print(f"process {os.getpid()}: {self.begin}")
                self.begin += 1
            sleep(self.timeout)

if __name__ == "__main__":
    lock=Lock()
    process1 = MyProcess(0, 20, 1, lock=lock)
    process2 = MyProcess(5, 30, 0.5, lock=lock)

    print("START")
    process1.start()
    process2.start()

    print("END")