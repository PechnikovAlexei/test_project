def decorator(function):
    def wrapper(*args, **kwargs):
        print("ARGS:", args)
        print("KWARGS:", kwargs)
        return function(*args, **kwargs)

    return wrapper

# @decorator - аналогично 13-й строке
def function(a, b):
    return a ** b

function = decorator(function) # @decorator
result = function(4, 5)
print("RESULT:", result)