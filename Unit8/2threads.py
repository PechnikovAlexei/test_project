from threading import Thread
from time import sleep

def func(name, start, end, timeout):
    while start < end:
        print(f"Thread {name}: {start}")
        start += 1
        sleep(timeout)

thread1 = Thread(target=func, args=("Func1", 0, 20, 1)) # Создание объектов для потока
thread2 = Thread(target=func, args=("Func2", 5, 30, 0.5), daemon=True) # daemon - главный поток не следит за дочерним, и по завершении главного остальные закончат работу

print("START")
thread1.start() # Запуск потоков
thread2.start()

func("Main", 0, 10, 1)

thread2.join() # Ожидание окончания потока
print("THREAD2 END")
thread1.join()
print("THREAD1 END")
print("END")