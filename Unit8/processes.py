# import _multiprocessing - низкоуровневая библиотека
from multiprocessing import Process
import os
from time import sleep

def func(start, end, timeout):
    while start < end:
        print(f"Process {os.getpid()}: {start}")
        start += 1
        sleep(timeout)

if __name__ == "__main__":
    process1 = Process(target=func, args=(0, 20, 1)) # 
    process2 = Process(target=func, args=(5, 30, 0.5)) 

    print("START")
    process1.start() # Запуск процессов
    process2.start()

    func(0, 10, 1)
    print("END")