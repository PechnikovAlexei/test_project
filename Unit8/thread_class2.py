from threading import Lock, Thread
from time import sleep

class MyThread(Thread):
    def __init__(self, name, end, timeout, daemon=False, lock=None):
        super().__init__(daemon=daemon)
        self.name = name
        self.end = end
        self.timeout = timeout
        self.lock = lock

    def run(self):
        global begin
        while begin < self.end:
            # with self.lock: Замена self.lock.acquire()
            self.lock.acquire()
            print(f"Thread {self.name}: {begin}")
            begin += 1
            self.lock.release()
            sleep(self.timeout)

lock=Lock()
begin = 0
thread1 = MyThread("1", 20, 0, lock=lock)
thread2 = MyThread("2", 30, 0, lock=lock)

print("START")
thread1.start()
thread2.start()

print("END")