import socket

sock = socket.socket() # По-умолчанию протокол TCP
print("$ Create socket")
sock.bind(("", 2000)) # Указывается порт и с какого ip ждем подключение
print("$ Bind to port 2000")
sock.listen(1) # Кол-во подключений
print("$ Set listen count to 1")

while True:
    conn, addr = sock.accept() # приглашение клиента conn - объект и его адрес
    print(f"Get connection from {addr[0]}:{addr[1]}")
    
    while True:
        try:
            data = conn.recv(1024)
            print(f"{addr[0]}:{addr[1]}> {data}")
            conn.send(b"Gotcha!") # отправка данных в ответ
        except ConnectionAbortedError:
            break

    conn.close()
    
socket.close()