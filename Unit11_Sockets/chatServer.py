import socket
from threading import Thread
t = True
def send():
    while True:
        if str(conn.send(input().encode("utf-8"))) in ("3"):
            break

def receive():
    while True:
        data = conn.recv(1024).decode("utf-8")
        print(data)
        if data in ("Bye"):
            conn.send(b"Bye!")
            break
        elif data in ("Bye!"):
            conn.send(b"")
            break

sock = socket.socket() # По-умолчанию протокол TCP
sock.bind(("", 2000)) # Указывается порт и с какого ip ждем подключение
sock.listen(1) # Кол-во подключений
conn, addr = sock.accept() # приглашение клиента conn - объект и его адрес
print(f"Get connection from {addr[0]}:{addr[1]}")

thread1 = Thread(target=send)
thread2 = Thread(target=receive)

thread2.start()
thread1.start() # отправка данных в ответ
# print(f"{addr[0]}:{addr[1]}> {data}")

thread2.join()
thread1.join()

sock.close()
conn.close()