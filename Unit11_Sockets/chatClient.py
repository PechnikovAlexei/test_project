import socket
from threading import Thread
from sys import getdefaultencoding

getdefaultencoding()
def send():
    while True:
        sdata = sock.send(input().encode("utf-8"))
        # print(sdata)
        if str(sdata) in ("3"):
            # print(sdata)
            break

def receive():
    while True:
        data = sock.recv(1024).decode("utf-8")
        print(data)
        if data in ("Bye"):
            sock.send(b"Bye!")
            break
        elif data in ("Bye!"):
            sock.send(b"")
            break

thread1 = Thread(target=send)
thread2 = Thread(target=receive)

sock = socket.socket()
print("Create socket")
sock.connect(("127.0.0.1", 2000)) # Куда коннектимся
print("Connected to localhost:2000")

thread1.start()
thread2.start()
#print(data)
thread2.join()
thread1.join()

sock.close()
