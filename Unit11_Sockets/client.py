import socket

sock = socket.socket()
print("Create socket")
sock.connect(("127.0.0.1", 2000)) # Куда коннектимся
print("Connected to localhost:2000")

sock.send(b"Hello, server")
print("Sent data to localhost:2000")

data = sock.recv(1024)
print("Get data from localhost:2000:")
print(data)
sock.close()
print("Close socket")