# coding=UTF-8
from .history import his

def calc(operation, x, y):
    if operation == "+":
        result = x + y
        print("Результат", x, operation, y, "=", result, end="\n\n", sep=" ")
        his(operation, x, y, result)
    elif operation == "-":
        result = x - y
        print("Результат", x, operation, y, "=", result, end="\n\n", sep=" ")
        his(operation, x, y, result)
    elif operation == "*":
        result = x * y
        print("Результат", x, operation, y, "=", result, end="\n\n", sep=" ")
        his(operation, x, y, result)
    elif operation == "/":
        result = x / y
        print("Результат", x, operation, y, "=", result, end="\n\n", sep=" ")
        his(operation, x, y, result)
    elif operation == "**":
        result = x ** y
        print("Результат", x, operation, y, "=", result, end="\n\n", sep=" ")
        his(operation, x, y, result)
