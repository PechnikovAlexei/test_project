# coding=UTF-8

print("""Программа Калькулятор
Программа производит операции с двумя числами x и y.
Введите соответствующее условное обозначение операции и два числа:
Сложение: +
Вычитание: -
Умножение: *
Деление: /
Возведение x в степень y: **
Просмотр истории операций: history
Выход: exit\n""")

from modulezz.ops import calc
while True:
    operation = input("Введите операцию: ")
    if operation not in ("history", "exit") and operation in ("+", "-", "*", "/", "**"):
        x = int(input("Введите первое число x: "))
        y = int(input("Введите второе число y: "))
        calc(operation, x, y)
    elif operation == "history":
        with open("History.txt", mode="rt") as f:
            print(f.read())
    elif operation == "exit":
        break
    else:
        print("Такой операции нет, выберите из предложенного списка")
