lst = [1, 2, 3, 4]
for element in lst:
    print(element, end=", ")

print("__iter__ in lst:", hasattr(lst, "__iter__"))
it = iter(lst)

for element in lst:
    try:
        print(next(it), end=", ")
        print(next(it), end=", ")
    except Exception as e:
        print("it ended:", e)
    print(element, end=", ")
