class RegularClass:
    pass

class SlotsClass:
    __slots__ = ("foo",)


class ChildSlotsClass(SlotsClass):
    pass


r_obj = RegularClass()
r_obj.foo = 5
print(r_obj.__dict__)
s_obj = SlotsClass()
s_obj.foo = 5
# print(s_obj.__dict__)
cs_obj = ChildSlotsClass() # указывать каждому классу slots
cs_obj.foo = 5
print(cs_obj.__dict__) # В dict попадают только новые методы, а foo не попал, т.к. он уже определен былв родительском классе
