def arguments(dynamic=False):
    def decorator(function):
        def wrapper(*args, **kwargs):
            if dynamic:
                self = args[0]
                _args = args[1:]
            else:
                _args = args
            print(f"args {function.__name__}: {_args}")
            print(f"kwargs {function.__name__}: {kwargs}")
            return function(*args, **kwargs)
        
        return wrapper
    return decorator

class Calculator:
    @arguments(True)
    def degree(self, digit, degree):
        return digit ** degree

    @arguments()    
    def div(a, b):
        return a / b

    def sum(self, a, b): # динамический метод - должен передаваться self
        return a + b
    
    @classmethod
    def sub(cls, a, b): # классовый метод - должен передаваться сам класс cls
        return a - b
    
    @staticmethod
    def mul(a, b): # статический метод
        return a * b


c = Calculator()
c.degree(3, 4)
Calculator.div(10, 5)