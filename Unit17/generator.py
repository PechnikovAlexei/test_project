def xrange(start, end, step):
    while start < end:
        yield start
        start += step

    return "stop!!!"

def fibonacci():
    prev, now = 0, 1
    while True:
        yield prev
        now, prev = prev + now, now


gen = xrange(1, 3, 0.5)
print("gen:", gen)

print(next(gen))
print(next(gen))
print(next(gen))
print(next(gen))
try:
    print(next(gen))
except:
    pass

print("loop: ", end="")
for element in xrange(1, 3, 0.5):
    print(element, end=", ")
print()