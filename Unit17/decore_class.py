class Decorator:
    def __new__(cls, _class):
        print("Decorate")
        return cls

@Decorator
class MyClass:
    def __init__(self):
        print("My Class")

MyClass = Decorator(MyClass)
print("-" *40)
# mc = MyClass()
