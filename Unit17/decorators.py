def arguments(function):
    def wrapper1(*args, **kwargs):
        print(f"args {function.__name__}:{args}")
        print(f"kwargs {function.__name__}:{kwargs}")
        return function(*args, **kwargs)
    return wrapper1

def result(promt):
    def decorator(function):
        def wrapper2(*args, **kwargs):
            result = function(*args, **kwargs)
            print(f"{promt} > result {function.__name__}: {result}")
            return result
        return wrapper2
    return decorator


@result("log")
@arguments # аналог div = arguments(div)
def div(a, b):
    return a / b

# div = arguments(div)
print(div(100, b=25))
